<?php

namespace Tests\Unit;

use App\Blog;
use App\Models\BlogModel;
use App\Repositories\BlogRepository;
use App\Services\BlogService;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Mockery as m;
use Tests\TestCase;

class BlogServiceTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */

    public function tearDown(): void
     {
         parent::tearDown();
         m::close();
     }

    public function testCreateBlogs()
    {
        /*
         * wtf is dit
         *
         *      $mock->shouldReceive('addKek');
            dd(method_exists($mock, $mock->addKek()));
         */
        $this->instance(BlogService::class, m::mock(BlogService::class, function ($mock) {
            $blogModel = new BlogModel();
            $mock->shouldReceive('createBlog')->andReturn($blogModel);
            $repoMock = $this->mock(BlogRepository::class);
            $service = new BlogService($repoMock);

            $this->assertInstanceOf(BlogModel::class, $service->createBlog());
            $this->assertEquals(gettype($service->createBlog()),gettype($mock->createBlog()));
        }));


    }

    public function testReadBlogs()
    {
        // same as CreateBlog , Returned een instance of een blogmodel,
        $this->instance(BlogService::class, m::mock(BlogService::class, function ($mock) {
            $model = new BlogModel();
            $mock->shouldReceive('createReadBlog')->andReturn($model);
            $repoMock = $this->mock(BlogRepository::class);
            $service = new BlogService($repoMock);

            $this->assertInstanceOf(BlogModel::class, $service->createReadBlog());
            $this->assertEquals(gettype($service->createReadBlog()),gettype($mock->createReadBlog()));
        }));
    }

//
//    public function testUpdateBlogs()
//    {
//        $this->assertTrue(true);
//    }

//    public function testDeleteBlogs()
//    {
//        $this->assertTrue(true);
//    }
}
