<?php

namespace Tests\Unit;

use App\Blog;
use App\Http\Controllers\BlogController;
use App\Mappers\BlogEloquentMapper;
use App\Models\BlogModel;
use App\Repositories\BlogRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Mockery as m;
use Tests\TestCase;

class BlogControllerTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testTheTestingMethodInController()
    {
        // dit moet post data nabootsen

//        $data = new BlogModel();
//        $data->setId(2);
//        $data->setTitle("heh");
//       $data->setContent("content");

        $this->instance(BlogController::class, m::mock(BlogController::class, function ($mock) {
            $repo = new BlogRepository();
            $mock->shouldReceive('test')->andReturn($repo->all());
            dd($mock->test());
        }));
    }
}
