<?php


namespace App\Mappers;


use App\Blog;
use App\Models\BlogModel;
use Illuminate\Support\Collection;

class BlogModelMapper
{
    public static function toBlogModelCollection(Collection $collection)
    {
        return $collection->map(function ($eloquentModel) {
            return self::toBlogModel($eloquentModel);
        });
    }

    public static function toBlogEloquentModel(BlogModel $blog)
    {
        $eloquent = new Blog();
        $eloquent->id = $blog->getId();
        $eloquent->title = $blog->getTitle();
        $eloquent->content = $blog->getContent();

        return $eloquent;
    }

}
