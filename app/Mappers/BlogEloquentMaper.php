<?php


namespace App\Mappers;

use Faker;
use App\Blog;
use App\Models\BlogModel;
use Illuminate\Support\Collection;

class BlogEloquentMapper
{
    public static function toEloquentModel(BlogModel $blogModel)
    {

        $title = 'title';
        $content = 'content';
        $blog = new Blog();
        $title = $blogModel->getTitle();
        $content = $blogModel->getContent();
        return $blog;
    }

    public static function toEloquentCollection(Collection $collection)
    {
        return $collection->map(function ($item) {
            return self::toEloquentModel($item);
        });
    }

}
