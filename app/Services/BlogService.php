<?php


namespace App\Services;


use App\Blog;
use App\Models\BlogModel;
use App\Repositories\Interfaces\BlogRepositoryInterface;

class BlogService implements BlogServiceInterface
{

    /**
     * @var BlogRepositoryInterface
     */
    private $blogRepository;

    public function __construct(BlogRepositoryInterface $blogRepository)
    {
        $this->blogRepository = $blogRepository;
    }


    /**
     * @param BlogModel $blogModel
     * @return Blog
     */
    public function createBlog(BlogModel $blogModel)
    {
        // TODO: Implement createBlog() method.
            return new Blog();
    }

    public function createReadBlog()
    {
        return new BlogModel();
    }

    public function createUpdateBlog()
    {
        // TODO: Implement createUpdateBlog() method.
    }

    public function createDeleteBlog()
    {
        // TODO: Implement createDeleteBlog() method.
    }

    public function addCoins(){

    }
}
