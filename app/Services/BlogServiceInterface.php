<?php


namespace App\Services;


interface BlogServiceInterface
{

    public function createBlog();
    public function createReadBlog();
    public function createUpdateBlog();
    public function createDeleteBlog();
}
