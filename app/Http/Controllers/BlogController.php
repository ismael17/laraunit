<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\BlogRepositoryInterface;
use App\Services\BlogService;
use App\Services\BlogServiceInterface;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    private $blogRepository;
    private $blogService;

    public function __construct(BlogRepositoryInterface $blogRepository, BlogService $blogService)
    {
        $this->blogRepository = $blogRepository;
        $this->blogService = $blogService;
    }

    public function index()
    {

    }

    public function test(){
        dd($this->blogService->createBlog());
    }

    public function show($id)
    {
        return 123;
    }

}
