<?php


namespace App\Repositories\Interfaces;


interface BlogRepositoryInterface
{
    public function all();
    public function find($id);
    public function deleteOldBlogs($id);

}
